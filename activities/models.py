from django.db import models
# Create your models here.
class Activity(models.Model):
    nama_kegiatan = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_kegiatan


class Participant(models.Model):
    nama_peserta = models.CharField(max_length=30)
    kegiatan = models.ForeignKey(Activity, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama_peserta
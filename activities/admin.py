from django.contrib import admin
from activities.models import Activity, Participant

admin.site.register(Activity)
admin.site.register(Participant)
# Register your models here.

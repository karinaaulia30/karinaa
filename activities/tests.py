from django.test import TestCase, Client
from django.urls.base import resolve
from .views import saveactivities
from .models import Activity, Participant

class ActivitiesUnitTest(TestCase):
    def test_activities_url_is_exist(self):
        response = Client().get('/activities')
        self.assertEqual(response.status_code, 200)
    
    def test_activities_content(self):
        response = Client().get('/activities')
        self.assertTemplateUsed(response, 'activities/activities.html', 'base.html')
        activities_content = response.content.decode('utf8')
        self.assertIn("Activities", activities_content)
        self.assertIn('<form action="saveactivities" method="POST">', activities_content)

    def test_saveactivities_using_func(self):
        found = resolve('/saveactivities')
        self.assertEqual(found.func, saveactivities)

    def test_form_saveactivities(self):
        self.client.post('/saveactivities', {'nama_kegiatan':'Mabar'})
        self.client.post('/saveparticipant/1', {'nama_peserta':'Karina'})
        jumlah = Activity.objects.all().count()
        self.assertEqual(jumlah, 1)
        hitung = Participant.objects.all().count()
        self.assertEqual(hitung, 1)
        
    def test_models(self):
        kegiatan = Activity.objects.create(nama_kegiatan="Mabar")
        peserta = Participant.objects.create(nama_peserta="Karina", kegiatan=kegiatan)
        jumlah = Activity.objects.all().count()
        self.assertEqual(jumlah, 1)
        hitung = Participant.objects.all().count()
        self.assertEqual(hitung, 1)
        self.assertEqual(str(kegiatan), "Mabar")
        self.assertEqual(str(peserta), "Karina")
    
    def test_form_activity_invalid(self):
        self.client.post('/saveactivities', {})
        jumlah = Activity.objects.filter(nama_kegiatan='Test nama').count()
        self.assertEqual(jumlah, 0)
    
    def test_form_participant_invalid(self):
        self.client.post('/saveparticipant/1', {})
        jumlah = Participant.objects.filter(nama_peserta='Test nama').count()
        self.assertEqual(jumlah, 0)
    
        

from django import forms


class Form_Kegiatan(forms.Form):
    nama_kegiatan = forms.CharField(label='Nama Kegiatan', max_length=64,
                                    widget=forms.TextInput())


class Form_Participants(forms.Form):
    nama_peserta = forms.CharField(label='Nama Peserta', max_length=64,
                                   widget=forms.TextInput())

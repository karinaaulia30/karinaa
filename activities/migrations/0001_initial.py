# Generated by Django 3.1.2 on 2020-11-12 15:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_peserta', models.CharField(max_length=30)),
                ('kegiatan', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='activities.activity')),
            ],
        ),
    ]

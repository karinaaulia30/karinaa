# Generated by Django 3.1.2 on 2020-11-13 06:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participant',
            name='kegiatan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='activities.activity'),
        ),
    ]

from django.shortcuts import render, HttpResponseRedirect
from .forms import Form_Participants, Form_Kegiatan
from .models import Activity, Participant

def activities(request):
    forms = {
        'form_kegiatan': Form_Kegiatan(),
        'form_peserta': Form_Participants(),
    }
    data = {
        'kegiatan': Activity.objects.all(),
        'peserta': Participant.objects.all(),
    }
    response = {
        'forms': forms,
        'data': data,
        'navbar':'activities'
    }
    return render(request, 'activities/activities.html', response)


def saveactivities(request):
    form = Form_Kegiatan(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        data_form = form.cleaned_data
        form = Activity(nama_kegiatan=data_form['nama_kegiatan'])
        form.save()
        return HttpResponseRedirect('/activities')
    else:
        return HttpResponseRedirect('/activities')

def saveparticipant(request, kegiatan_id):
    form = Form_Participants(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        kegiatan = Activity.objects.get(id=kegiatan_id)
        data_form = form.cleaned_data
        data_input = Participant()
        data_input.nama_peserta = data_form['nama_peserta']
        data_input.kegiatan = kegiatan
        data_input.save()
        return HttpResponseRedirect('/activities')
    else:
        return HttpResponseRedirect('/activities')

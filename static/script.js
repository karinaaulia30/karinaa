$( "#my-accordion" ).accordion({
  collapsible:true,
  active: false
});

// move up:
function moveup(head, drop) {
  $(head).prev().insertAfter($(drop));
  $(head).prev().insertAfter($(drop));
  $( "#my-accordion" ).accordion({
    active: false
  });
}
// move down:
function movedown(head, drop) {
  $(drop).next().insertBefore($(head));
  $(drop).next().insertBefore($(head));
  $( "#my-accordion" ).accordion({
    active: false
  });
}
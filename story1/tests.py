from django.test import TestCase, Client

# Create your tests here.
class Story1UnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)
    
    def test_profile_content(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'story1/profile.html')
        profile_content = response.content.decode('utf8')
        self.assertIn("Who Am I?", profile_content)
        self.assertIn("Hobbies", profile_content)
        self.assertIn("Experiences", profile_content)
        self.assertIn("Education", profile_content)
        self.assertIn('<div id="my-accordion">', profile_content)
        self.assertIn('Activities', profile_content)
        self.assertIn('Organizations', profile_content)
        self.assertIn('Achievements', profile_content)
        self.assertIn('▲', profile_content)
        self.assertIn('▼', profile_content)

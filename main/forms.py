from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import Course

class Input_Form(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['nama_matkul', 'nama_dosen', 'jumlah_sks', 'deskripsi', 'semester_tahun', 'ruang_kelas']
    error_messages = {
		'required' : 'Please Type'
	}
	
    nama_matkul = forms.CharField(label='Mata Kuliah', required=True, max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Nama Mata Kuliah'}))
    nama_dosen = forms.CharField(label='Dosen', required=True, max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Nama Dosen'}))
    jumlah_sks = forms.IntegerField(label='Jumlah SKS', required=True, widget=forms.TextInput(attrs={'placeholder' : 'Jumlah SKS (angka saja)'}))
    deskripsi = forms.CharField(label='Deskripsi', required=True, max_length=50, widget=forms.Textarea(attrs={'placeholder' : 'Deskripsi'}))
    semester_tahun = forms.CharField(label='Semester Gasal/Genap dan Tahun', required=True, max_length=15, widget=forms.TextInput(attrs={'placeholder' : '(misal: Gasal 2019/2020)'}))
    ruang_kelas = forms.CharField(label='Kelas', required=False, max_length=10, widget=forms.TextInput(attrs={'placeholder' : 'Ruang Kelas'}))

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username','password1','password2']
    username = forms.CharField(required=True, max_length=20, widget=forms.TextInput(attrs={'placeholder' : 'Username', 'class' : 'form-control'}))
    password1 = forms.CharField(required=True, min_length=8, widget=forms.PasswordInput(attrs={'placeholder' : 'Password', 'class' : 'form-control'}))
    password2 = forms.CharField(required=True, min_length=8, widget=forms.PasswordInput(attrs={'placeholder' : 'Password Confirmation', 'class' : 'form-control'}))
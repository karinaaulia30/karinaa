from django.db import models

# Create your models here.
class Course(models.Model):
    nama_matkul = models.CharField(max_length=30, blank=True)
    nama_dosen = models.CharField(max_length=30, blank=True)
    jumlah_sks = models.IntegerField(null=True)
    deskripsi = models.CharField(max_length=50, blank=True)
    semester_tahun = models.CharField(max_length=15, blank=True)
    ruang_kelas = models.CharField(max_length=10, blank=True)

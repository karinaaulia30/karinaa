from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import response
from django.shortcuts import redirect, render
from django.http import HttpResponseRedirect

from .forms import Input_Form, CreateUserForm
from .models import Course

def home(request):
    return render(request, 'main/home.html', {'navbar':'home'})

def moments(request):
    return render(request, 'main/moments.html', {'navbar':'moments'})

def contact(request):
    return render(request, 'main/contact.html', {'navbar':'contact'})

def course(request):
    courses = Course.objects.all()
    response = {'input_form' : Input_Form, 'navbar':'course', 'courses':courses}
    return render(request, 'main/course.html', response)

def savecourse(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/course')
    else:
        return HttpResponseRedirect('/course')

def deletecourse(request, nama_matkul):
    course = Course.objects.filter(nama_matkul=nama_matkul)
    course.delete()
    return HttpResponseRedirect("/course")

def coursedetails(request, nama_matkul):
    course = Course.objects.get(nama_matkul=nama_matkul)
    response = {'course':course}
    return render(request,"main/details.html", response)

def register(request):
    if (request.method == 'POST'):
        form = CreateUserForm(request.POST)
        if (form.is_valid()):
            form.save()
            return redirect('/login')
    else:
        form = CreateUserForm()
    response = {'form':form}
    return render(request,"main/register.html", response)

def loginuser(request):
    if (request.method == 'POST'):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.info(request, 'Username or password is incorrect')

    return render(request,"main/login.html")

def logoutuser(request):
    logout(request)
    return redirect('/login')
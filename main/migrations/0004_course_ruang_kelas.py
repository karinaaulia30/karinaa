# Generated by Django 3.1.2 on 2020-10-13 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20201013_2117'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='ruang_kelas',
            field=models.CharField(blank=True, max_length=10),
        ),
    ]

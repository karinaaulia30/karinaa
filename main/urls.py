from django.urls import path

from . import views
app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('moments', views.moments, name='moments'),
    path('contact', views.contact, name='contact'),
    path('course', views.course, name='course'),
    path('savecourse', views.savecourse),
    path('deletecourse/<str:nama_matkul>', views.deletecourse),
    path('coursedetails/<str:nama_matkul>', views.coursedetails),
    path('register', views.register, name='register'),
    path('login', views.loginuser, name='login'),
    path('logout', views.logoutuser, name='logout'),
]

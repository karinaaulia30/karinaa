from django.test import TestCase, Client
from django.urls.base import resolve
from . import views
from main.models import Course
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

class MainUnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_home_content(self):
        response = Client().get('/')
        home_content = response.content.decode('utf8')
        self.assertIn("Welcome to My Website!", home_content)
        self.assertIn('<span class="fa fa-user"></span> Sign Up</a>', home_content)
        self.assertIn('<span class="fa fa-user"></span> Sign Up</a>', home_content)
        

    def test_moments(self):
        response = Client().get('/moments')
        self.assertTemplateUsed(response, 'main/moments.html', 'base.html')
        moments_content = response.content.decode('utf8')
        self.assertIn("Moments", moments_content)

    def test_contact(self):
        response = Client().get('/contact')
        self.assertTemplateUsed(response, 'main/contact.html', 'base.html')
        contact_content = response.content.decode('utf8')
        self.assertIn('<button type="button" class="btn btn-outline-dark">KAulia56@gmail.com</button>', contact_content)
        self.assertIn('<button type="button" class="btn btn-outline-dark">@karina.aulia</button>', contact_content)
        self.assertIn('<button type="button" class="btn btn-outline-dark">Karina Aulia Putri</button>', contact_content)

    def test_course(self):
        response = Client().get('/course')
        self.assertTemplateUsed(response, 'main/course.html', 'base.html')
        course_content = response.content.decode('utf8')
        self.assertIn("My Courses", course_content)
        self.assertIn('<form action="savecourse" method="POST">', course_content)
    
    def test_course_using_func(self):
        found = resolve('/course')
        self.assertEqual(found.func, views.course)
    
    def test_savecourse_using_func(self):
        found = resolve('/savecourse')
        self.assertEqual(found.func, views.savecourse)

    def test_savecourse_valid(self):
        self.client.post('/savecourse', {'nama_matkul':'ppw','nama_dosen':'pa adin','jumlah_sks':'3','deskripsi':'teks','semester_tahun':'gasal 2020/2021','ruang_kelas':'E'})
        jumlah = Course.objects.count()
        self.assertEqual(jumlah, 1)

    def test_savecourse_invalid(self):
        self.client.post('/savecourse', {})
        jumlah = Course.objects.count()
        self.assertEqual(jumlah, 0)

    def test_coursedetails_using_func(self):
        found = resolve('/coursedetails/<str:nama_matkul>')
        self.assertEqual(found.func, views.coursedetails)
    
    def test_models(self):
        Course.objects.create(nama_matkul = "PPM", nama_dosen = "Bu Thia", jumlah_sks = 3, deskripsi = "PPM keren", semester_tahun = "Gasal 2019/2020", ruang_kelas = "C")
        hitung_jumlah = Course.objects.all().count()
        self.assertEqual(hitung_jumlah, 1)

    def test_deletecourse_using_func(self):
        found = resolve('/deletecourse/<str:nama_matkul>')
        self.assertEqual(found.func, views.deletecourse)
        course = Course.objects.filter(nama_matkul="PPM")
        course.delete()

    # register
    def test_register_page(self):
        response = Client().get('/register')
        self.assertEqual(response.status_code, 200)

class LogInTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', password='12test12testtest')
        self.user.save()
        
    def test_authenticated(self):
        User.objects.create(username="testuser", password="test1234")
        response = Client().post('/login', {'username': 'testuser', 'password': 'test1234'})
        self.assertEqual(response.status_code, 200)

    def tearDown(self):
        self.user.delete()

    def test_correct(self):
        user = authenticate(username='test', password='12test12testtest')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='wrong', password='12test12testtest')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_password(self):
        user = authenticate(username='test', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)

    # def test_login_page(self):
    #     response = Client().get('/login')
    #     self.assertEqual(response.status_code, 200)

    

    
    
    
from django.http import JsonResponse
from django.shortcuts import render
import requests
import json
# Create your views here.
def searchbooks(request):
    return render(request, 'story8/searchbooks.html', {'navbar':'searchbooks'})

def data(request):
    arg = request.GET['q']
    my_url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    obj = requests.get(my_url)
    data = json.loads(obj.content)
    return JsonResponse(data, safe=False)
from django.test import TestCase, Client
from django.urls.base import resolve
from .views import searchbooks
import json
# Create your tests here.
class Story8UnitTest(TestCase):
    def test_searchbooks_url_is_exist(self):
        response = Client().get('/searchbooks')
        self.assertEqual(response.status_code, 200)

    def test_searchbooks_content(self):
        response = Client().get('/searchbooks')
        self.assertTemplateUsed(response, 'story8/searchbooks.html', 'base.html')
        searchbooks_content = response.content.decode('utf8')
        self.assertIn('input id="search"', searchbooks_content)
    
    def test_searchbooks_using_func(self):
        found = resolve('/searchbooks')
        self.assertEqual(found.func, searchbooks)

    def test_search(self):
        self.client.get('/data',{'my_url':'https://www.googleapis.com/books/v1/volumes?q=abc'})

    def test_your_test(self):
        test2 = {
            "1": {
                "title": "judul",
                "thumbnail": "https://i.ytimg.com/vi/9hUhwVVUesA/maxresdefault.jpg",
            }
        }
        response = self.client.post('data/',
                                    json.dumps(test2),
                                    content_type="application/json")
from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('searchbooks', views.searchbooks, name='searchbooks'),
    path('data/', views.data)
]
